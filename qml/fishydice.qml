import QtQuick 2.0
import Sailfish.Silica 1.0
import "ui"

ApplicationWindow
{
    initialPage: Component { MainPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: Orientation.All
    _defaultPageOrientations: Orientation.All

    DiceController {
        id: diceController
    }
}


