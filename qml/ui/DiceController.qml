import QtQuick 2.0

QtObject {
    signal diceThrown(int index)

    function randint(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function newScore(sides){
        return randint(1, sides)
    }

    function throwDice(listModel, index) {
        var element = listModel.get(index)
        if (element.active) {
            var newVal = newScore(element.sides)
            element.score = newVal
            diceThrown(index)
        }
    }

    function throwAllActiveDice(listModel) {
        for (var i = 0; i < listModel.count; ++i) {
            throwDice(listModel, i)
        }
    }

    property var availableDice: [4, 6, 8, 10, 12, 20]
}

