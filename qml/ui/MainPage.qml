import QtQuick 2.0
import QtQuick.Layouts 1.0
import Sailfish.Silica 1.0
import org.nemomobile.configuration 1.0
import "constants.js" as Constants


Page {
    id: page

    property int gridDimension: gridSize.value

    property ListModel diceModel: ListModel {}
    // TODO store the model

    ConfigurationValue {
        id: gridSize
        key: Constants.gridSizeKey
        defaultValue: 2
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
            MenuItem {
                text: qsTr("Add a dice")
                onClicked: pageStack.push(diceSetupDialog)
            }
        }

        contentHeight: column.height

        ColumnLayout {
            id: column

            width: page.width
            height: page.height

            spacing: Theme.paddingLarge

            PageHeader {
                id: header
                title: qsTr("Fishy Dice")
                width: parent.width - icon.width
                IconButton {
                    id: icon
                    icon.source: "image://theme/icon-m-sync"
                    anchors.left: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    onClicked: {
                        diceController.throwAllActiveDice(diceModel)
                    }
                }
            }


            ViewPlaceholder {
                enabled: diceView.count === 0
                text: qsTr("No items yet")
                hintText: qsTr("Pull down to add items")
            }

            SilicaGridView {
                id: diceView

                Layout.preferredWidth: parent.width
                Layout.fillHeight: true

                model: diceModel

                cellWidth: width / gridDimension
                cellHeight: height / gridDimension

                delegate: BackgroundItem {

                    id: bgItem
                    width: diceView.cellWidth
                    height: diceView.cellHeight

                    Dice {
                        id: dice
                        score: model.score;
                        sides: model.sides
                        active: model.active
                        anchors.centerIn: parent
                    }

                    Label {
                        anchors.top: dice.bottom
                        anchors.horizontalCenter: dice.horizontalCenter
                        anchors.margins: 10
                        text: "D" + model.sides
                    }

                    ContextMenu {
                        id: contextMenu
                        parent: bgItem
                        anchors {
                            left: bgItem.left
                            right: bgItem.right
                            //bottom: bgItem.bottom
                        }

                        RemorseItem {
                            id: remorse
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                        }

                        MenuItem {
                            text: qsTr("Remove")
                            onClicked: remorse.execute(bgItem,
                                                        "Deleting",
                                                        bgItem.removeMe)
                        }

                        MenuItem {
                            text: dice.active ? qsTr("Deactivate") : qsTr("Activate")
                            onClicked: {
                                diceModel.get(model.index).active = !dice.active
                            }
                        }
                    }

                    onClicked: diceController.throwDice(diceModel, model.index)
                    onPressAndHold: contextMenu.show(this)

                    function removeMe() {
                        diceModel.remove(model.index)
                    }
                }
            }
        }
    }

    Component {
        id: diceSetupDialog
        DiceSetupDialog {
            onAccepted: {
                var copiesNum = parseInt(copies)
                for (var i = 0; i < copiesNum; ++i) {
                    diceModel.append({"sides": sides,
                                         "active": active,
                                         "score": diceController.newScore(sides)
                                     })
                    diceController.diceThrown(diceModel.count - 1)
                }
            }
        }
    }
}


