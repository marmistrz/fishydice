import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.Layouts 1.0

RowLayout {
    property int sides

    Label {
        text: "D" + sides
        Layout.fillWidth: true
    }

    Label {
        text: "?"
    }
}

