import QtQuick 2.2
import Sailfish.Silica 1.0

/** This will not work outside of a delegate*/
Rectangle {
    id: root

    property int dimension: 100 // FIXME tablet/phone responsiveness and different shapes
    property int sides
    property int score
    property bool active

    function animate() {
        anim.running = true;
    }

    radius: 10
    color: "white"
    width: dimension
    height: dimension
    opacity: active ? 1.0 : 0.5

    Label {
        anchors.centerIn: parent
        color: "black"
        text: score
        visible: !anim.running
    }

    RotationAnimator {
        id: anim
        easing.type: Easing.OutQuad
        target: root
        from: 0
        to: 360
        duration: 1000
    }

    Connections {
        target: diceController
        onDiceThrown: {
            if (index === model.index) {
                animate()
            }
        }
    }

}

