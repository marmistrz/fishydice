import QtQuick 2.0
import QtQuick.Layouts 1.0
import Sailfish.Silica 1.0

Dialog {
    property int sides: diceController.availableDice[slider.value]
    property alias active: activationSwitch.checked
    property alias copies: copiesField.text

    Column {
        width: parent.width

        DialogHeader { }

        Slider {
            id: slider
            width: parent.width
            label: "Sides"
            stepSize: 1
            minimumValue: 0
            maximumValue: diceController.availableDice.length - 1
            valueText: diceController.availableDice[value]
        }

        TextSwitch {
            id: activationSwitch
            text: "Active"
            description: "Inactive dices won't be thrown"
            checked: true
        }

        RowLayout {

            anchors {
                margins: Theme.paddingLarge
                left: parent.left
                right: parent.right
            }

            spacing: Theme.paddingSmall

            Label {
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Number of copies: ")
            }

            TextField {
                id: copiesField
                anchors.verticalCenter: parent.verticalCenter
                inputMethodHints: Qt.ImhDigitsOnly
                text: "1"
                validator: IntValidator { bottom: 1 }
                Layout.fillWidth: true
            }
        }
    }
}
