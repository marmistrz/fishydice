import QtQuick 2.0
import QtQuick.Layouts 1.0
import Sailfish.Silica 1.0
import org.nemomobile.configuration 1.0
import "constants.js" as Constants

Page {
    ConfigurationValue {
        id: gridSize
        key: Constants.gridSizeKey
        defaultValue: 2
    }

    ColumnLayout {
        width: parent.width
        Slider {
            label: qsTr("Size of the grid")
            minimumValue: 1
            maximumValue: 6
            stepSize: 1
            value: defaultValue(gridSize.value)
            valueText: value
            width: parent.width

            onValueChanged: {
                gridSize.value = value
            }
        }
    }

    function defaultValue(foo) {
        return foo;
    }
}
